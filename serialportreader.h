#ifndef SERIALPORTREADER_H
#define SERIALPORTREADER_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QFile>
#include <QTime>
#include <QDir>
#include <QTextStream>

struct Settings {
    QString name;
    qint32 baudRate;
    QString stringBaudRate;
    QSerialPort::DataBits dataBits;
    QString stringDataBits;
    QSerialPort::Parity parity;
    QString stringParity;
    QSerialPort::StopBits stopBits;
    QString stringStopBits;
    QSerialPort::FlowControl flowControl;
    QString stringFlowControl;
    bool localEchoEnabled;
};

class SerialPortReader : public QObject
{
    Q_OBJECT
    QSerialPort *serialPort;
    Settings portSettings;
    void readSerial();
    QDir logsDir;
    QFile logsSerial;

public:
    explicit SerialPortReader(Settings portSettings, QObject *parent = nullptr);
    bool openPort();
    void closePort();
    QString getErrorString();
    void setLogDir(QString dir);

signals:

};

#endif // SERIALPORTREADER_H
