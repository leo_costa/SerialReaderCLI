#include "serialportreader.h"

void SerialPortReader::readSerial()
{
    QByteArray data = serialPort->readAll();
    QTextStream console_out(stdout);

    for(unsigned char byte : qAsConst(data))
    {
        if((byte < 32 || byte > 126) && byte != '\n')
        {
//             console_out << QString("");
            console_out << QString("%1").arg(byte & 0xFF, 2, 16);
        }
        else
        {
            console_out << QString(byte);
        }
    }

    if(logsSerial.isOpen())
    {
//         QTextStream stream(&logsSerial);
//         stream << QString::fromLocal8Bit(data);
        logsSerial.write(data);
//        stream << "[" << QTime::currentTime().toString("hh:mm:ss:zzz") << "] " << QString::fromLocal8Bit(data);
    }
}

SerialPortReader::SerialPortReader(Settings portSettings, QObject *parent) : QObject(parent)
{
     serialPort = nullptr;
     serialPort = new QSerialPort(this);
     this->portSettings = portSettings;

     connect(serialPort, &QSerialPort::readyRead, this, &SerialPortReader::readSerial);
}

bool SerialPortReader::openPort()
{
    serialPort->setPortName(portSettings.name);
    serialPort->setBaudRate(portSettings.baudRate);

    serialPort->setDataBits(QSerialPort::Data8);
    serialPort->setParity(QSerialPort::NoParity);
    serialPort->setStopBits(QSerialPort::OneStop);
    serialPort->setFlowControl(QSerialPort::NoFlowControl);

    if(serialPort->open(QIODevice::ReadOnly))
    {
        logsSerial.open(QIODevice::WriteOnly);
        return true;
    }
    return false;
}

void SerialPortReader::closePort()
{
    if(serialPort->isOpen())
        serialPort->close();
    if(logsSerial.isOpen())
        logsSerial.close();
}

QString SerialPortReader::getErrorString()
{
    return serialPort->errorString();;
}

void SerialPortReader::setLogDir(QString dir)
{
    QString currentTime = QTime::currentTime().toString("hh:mm");

    if(!dir.endsWith("/"))
        dir.append("/");

    logsDir = QDir(dir+currentTime+".log");

    logsSerial.setFileName(logsDir.path());
}
