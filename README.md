# Serial Reader CLI

## Usage

Read serial port and save to log file

```shell
./serialReaderCLI --port-name ttyUSB0 --port-baud 115200 --log-dir ~/path_to_log_dir
```

 Show available ports

 ```shell
./serialReaderCLI -l
 ```
