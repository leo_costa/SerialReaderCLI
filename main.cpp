#include <QCoreApplication>
#include <iostream>
#include <QCommandLineParser>
#include <QTextStream>

#include "serialportreader.h"

#include <future>
#include <QTimer>

QVector<QStringList> getPortInfo();

void createParser(QCommandLineParser *parser);

void listPorts(QTextStream *console_out);

void listBaudRates(QTextStream *console_out, const QVector<qint32> &validBaud);


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setApplicationVersion("1.7");
    a.setApplicationName("Serial Reader Logger - CLI");

    QCommandLineParser *parser = nullptr;
    parser = new QCommandLineParser();

    QVector<qint32> validBaud = {9600, 19200, 38400, 74880, 115200, 230400};

    QTextStream console_out(stdout);

    bool exitFlag = false;

    auto f = std::async(std::launch::async, [&exitFlag]{ std::getchar();
                                                          exitFlag = true; });
    QTimer exitTimer;
    exitTimer.setInterval(200);
    exitTimer.setSingleShot(false);

    QObject::connect(&exitTimer, &QTimer::timeout,
                     [&a,&exitFlag] { if (exitFlag)
                                          a.quit(); } );

    exitTimer.start();

    // Process the actual command line arguments given by the user
    createParser(parser);
    parser->process(a);

    bool listP = parser->isSet("l");
    QString name = parser->value("n");
    QString logdir = parser->value("d");
    qint32 baud = parser->value("b").toInt();

    int ret = 0;

    if(listP)
    {
        listPorts(&console_out);
        listBaudRates(&console_out, validBaud);
    }
    else if(!name.isEmpty() && validBaud.contains(baud) && !logdir.isEmpty())
    {
        console_out << "Selected port:" << name <<
                       " | Baud rate:" << QString::number(baud) << Qt::endl;
        Settings portSettings;
        portSettings.name = name;
        portSettings.baudRate = baud;

        SerialPortReader serialReader(portSettings);
        serialReader.setLogDir(logdir);

        bool ok = serialReader.openPort();

        if(ok)
        {
            console_out << "Connected! Press any key and enter to exit" << Qt::endl;
            ret = a.exec();
            std::cout.flush();
            f.wait();
            serialReader.closePort();
        }
        else
        {
            console_out << "Failed to connect to port! Error: " <<
                           serialReader.getErrorString() << Qt::endl;
        }
    }
    else
    {
        console_out <<
            "Please provide a valid serial port name, baud rate and path for the logs!" << Qt::endl;
         ret = 0;
    }

    return ret;
}


QVector<QStringList> getPortInfo()
{
    QVector<QStringList> portsInfo; portsInfo.clear();

    QString description;
    QString manufacturer;
    QString serialNumber;
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
    {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
        serialNumber = info.serialNumber();
        list << info.portName()
             << (!description.isEmpty() ? description : "")
             << (!manufacturer.isEmpty() ? manufacturer : "")
             << (!serialNumber.isEmpty() ? serialNumber : "")
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : "")
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : "");
        portsInfo.append(list);
    }
    return portsInfo;
}


void createParser(QCommandLineParser *parser)
{

    parser->setApplicationDescription("Serial Reader CLI");
    parser->addHelpOption();
    parser->addVersionOption();

    QCommandLineOption portName(QStringList() << "n" << "port-name",
                QCoreApplication::translate("main", "Serial Port Name"),
                QCoreApplication::translate("main", "name"));
    parser->addOption(portName);

    QCommandLineOption portBaud(QStringList() << "b" << "port-baud",
            QCoreApplication::translate("main", "Serial Port Baud Rate"),
            QCoreApplication::translate("main", "baud"));
    parser->addOption(portBaud);

    QCommandLineOption logDir(QStringList() << "d" << "log-dir",
            QCoreApplication::translate("main", "Directory to save log from serial"),
            QCoreApplication::translate("main", "dir"));
    parser->addOption(logDir);

    QCommandLineOption listPorts(QStringList() << "l" << "list-ports",
            QCoreApplication::translate("main", "List available serial ports"));
    parser->addOption(listPorts);
}


void listPorts(QTextStream *console_out)
{
    QVector<QStringList> portsInfos = getPortInfo();

    for(int i = 0; i < portsInfos.size(); ++i)
    {
        *console_out << "Port[" << QString::number(i) << "] - Name: " <<
                        portsInfos.at(i).at(0) << " - Description: " <<
                        portsInfos.at(i).at(1) << Qt::endl;
    }
    return;
}


void listBaudRates(QTextStream *console_out, const QVector<qint32> &validBaud)
{
    *console_out << "Available Baud Rates: ";

    for(int i = 0; i < validBaud.size(); ++i)
    {
        *console_out << QString::number(validBaud.at(i)) << " ";
    }
    *console_out << Qt::endl;
}
